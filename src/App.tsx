import React from 'react';

import { Board } from './containers';

import { GameContextProvider } from './contexts/Board.context';

const App: React.FC = () => {
  return (
    <GameContextProvider>
      <div className="App">
        <Board />
      </div>
    </GameContextProvider>
  );
};

export default App;
