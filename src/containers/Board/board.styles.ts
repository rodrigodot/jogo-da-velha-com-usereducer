import styled from 'styled-components';

export const StyledBoardWraper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background-color: #008040;
  min-height: 100vh;
`;

export const StyledPlayer = styled.h3`
  color: #fff;
`;

export const StyledWinner = styled.h2`
  color: #fff;
  min-height: 34px;
`;

export const StyledBoardContaner = styled.h3`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  max-width: 900px;
`;

export const StyledHitoryContaner = styled.h5`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  align-items: flex-start;
  width: calc(100vw - 33vw);
`;
