import React, { FC, useContext } from 'react';

import {
  StyledBoardWraper,
  StyledPlayer,
  StyledBoardContaner,
  StyledWinner,
  StyledHitoryContaner,
} from './board.styles';

import { Square, Button, ListItem } from '../../components';
import { GameContext } from '../../contexts/Board.context';
import getWinner from '../../utils/getWinner';

export const Board: FC = () => {
  const {
    dispatch = () => {},
    state: { squares = [], currentPlayer, history, isWinner },
  } = useContext(GameContext);

  const handleClick = (index: number): void => {
    const newSquares = [...squares];
    newSquares[index] = currentPlayer;

    dispatch({ type: 'UPDATE_PLAYER' });
    dispatch({
      type: 'UPDATE_SQUARES',
      payload: newSquares,
    });
    dispatch({
      type: 'UPDATE_HISTORY',
      payload: `Casa ${index + 1} - Jogador: ${currentPlayer}`,
    });

    const winner = getWinner(newSquares);
    if (winner) {
      dispatch({
        type: 'UPDATE_WINNER',
        payload: currentPlayer,
      });
    }
  };

  return (
    <StyledBoardWraper>
      <StyledPlayer>Próximo jogador: {currentPlayer}</StyledPlayer>
      <StyledWinner>
        {isWinner ? `Vencedor: Jogador ${isWinner}` : ''}
      </StyledWinner>

      <StyledBoardContaner>
        {!!squares?.length &&
          squares.map((square: string, index: number) => {
            return (
              <Square
                key={index}
                title={square}
                onClick={() => handleClick(index)}
                squareProps={{
                  disabled: !!square || !!isWinner,
                }}
              />
            );
          })}
      </StyledBoardContaner>
      <Button
        title="Resetar"
        onClick={() => dispatch({ type: 'RESET' })}
      />

      <StyledHitoryContaner>
        {!!history.length &&
          history.map((item: string, index: number) => {
            return (
              <ListItem
                key={index}
                index={`${index}º`}
                details={item}
              />
            );
          })}
      </StyledHitoryContaner>
    </StyledBoardWraper>
  );
};
