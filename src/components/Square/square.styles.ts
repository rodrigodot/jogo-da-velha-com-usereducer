import styled from 'styled-components';

export const StyledSquare = styled.button`
  background-color: #0080ff;
  color: #fff;

  margin: 0;
  font-size: 18vw;
  width: 22vw;
  height: 22vw;
  border: 2px solid #fff;
  cursor: pointer;
  &:hover {
    opacity: 0.5;
  }
  &:disabled {
    opacity: 0.8;
  }
  transition: background-color 4s;
`;
