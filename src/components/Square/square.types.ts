import React from 'react';

export interface SquareProtocol {
  title: string;
  onClick: CallableFunction;
  squareProps?: React.ButtonHTMLAttributes<HTMLButtonElement>;
  squareStyles?: React.CSSProperties;
}
