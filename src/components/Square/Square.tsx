import React, { FC } from 'react';

import { SquareProtocol } from './square.types';
import { StyledSquare } from './square.styles';

export const Square: FC<SquareProtocol> = ({
  title,
  onClick,
  squareStyles,
  squareProps,
}: SquareProtocol) => {
  return (
    <StyledSquare
      {...squareProps}
      style={squareStyles}
      onClick={() => onClick()}
      color={squareStyles ? squareStyles.color : '#0080ff'}
    >
      {title}
    </StyledSquare>
  );
};
