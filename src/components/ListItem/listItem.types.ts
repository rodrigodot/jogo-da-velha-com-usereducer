export interface ListItemProtocol {
  index: string;
  details: string | number;
}
