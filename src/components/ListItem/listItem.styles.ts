import styled from 'styled-components';

export const StyledWraper = styled.div`
  margin: 0.5em 0.3em;
  font-size: 1em;
  padding: 0.5em 1em;
  border: 2px solid #fff;
  border-radius: 0.3em;
  transition: background-color 4s;
  color: #fff;
`;
