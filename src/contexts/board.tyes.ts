import { Dispatch } from 'react';

export interface StateProps {
  currentPlayer: string;
  isWinner: string;
  history: Array<string>;
  squares: Array<string>;
}

export interface DispatchProps {
  type: string;
  payload: null | Array<string> | string;
}

export type GameContextType = {
  state: StateProps;
  dispatch?: Dispatch<any>;
};
