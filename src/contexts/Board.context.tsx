/* eslint-disable no-case-declarations */
import React, { createContext, useReducer } from 'react';
import { StateProps, GameContextType } from './board.tyes';

const INITIAL_STATE = {
  squares: Array(9).fill(''),
  currentPlayer: 'X',
  isWinner: '',
  history: [],
};

export const GameContext = createContext<GameContextType>({
  state: INITIAL_STATE,
});

const reducer = (state: StateProps, action: any): StateProps => {
  switch (action.type) {
    case 'UPDATE_PLAYER': {
      const player = state.currentPlayer === 'X' ? 'O' : 'X';
      return { ...state, currentPlayer: player };
    }
    case 'UPDATE_WINNER': {
      return { ...state, isWinner: action.payload };
    }
    case 'UPDATE_SQUARES': {
      return { ...state, squares: action.payload };
    }
    case 'UPDATE_HISTORY': {
      const newHistory = [...state.history];
      newHistory.push(action.payload);
      return { ...state, history: newHistory };
    }
    case 'RESET': {
      return INITIAL_STATE;
    }
    default:
      return state;
  }
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const GameContextProvider = (props: {
  children: React.ReactChild;
}) => {
  const [state, dispatch] = useReducer(reducer, INITIAL_STATE);

  return (
    <GameContext.Provider value={{ state, dispatch }}>
      {props.children}
    </GameContext.Provider>
  );
};
